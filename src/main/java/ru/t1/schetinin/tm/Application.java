package ru.t1.schetinin.tm;

import ru.t1.schetinin.tm.api.ICommandRepository;
import ru.t1.schetinin.tm.constant.ArgumentConst;
import ru.t1.schetinin.tm.constant.CommandConst;
import ru.t1.schetinin.tm.model.Command;
import ru.t1.schetinin.tm.repository.CommandRepository;
import ru.t1.schetinin.tm.util.TerminalUtil;

import static ru.t1.schetinin.tm.util.FormatByteUtil.formatByte;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("Current program arguments are not correct...");
        System.exit(1);
    }

    private static void showSystemInfo(){
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorCount);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();

        System.out.println("MAX MEMORY: " + formatByte(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatByte(totalMemory));
        System.out.println("FREE MEMORY: " + formatByte(freeMemory));
        System.out.println("USED MEMORY: " + formatByte(totalMemory - freeMemory));
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.err.println("Current command is not correct...");
    }

    private static void processCommand(final String argument) {
        switch (argument) {
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikita Schetinin");
        System.out.println("E-mail: shetinin86@bk.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        for (Command command: COMMAND_REPOSITORY.getCommands()) System.out.println(command);
    }

}